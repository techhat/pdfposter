Examples for automatic scaling
------------------------------------

* For printing 2 *portrait* A4 pages high (approx. 58cm) and let
  pdfposter determine how many portrait pages wide, specify a large
  number of *vertical* pages. eg:

     :pdfposter -p999x2a4 testpage-wide.pdf out.pdf:

* For printing 2 *landscape* A4 pages high (approx. 20cm) and let
  pdfposter determine how many landscape pages wide, specify a large
  number of *horizontal* pages. eg:

     :pdfposter -p2x999a4 testpage-wide.pdf out.pdf:


.. Emacs config:
 Local Variables:
 mode: rst
 End:
