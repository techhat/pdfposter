Development
===============================


The source of |pdfposter| and its siblings is maintained at
`GitLab <https://gitlab.com>`_. Patches and pull-requests
are hearty welcome.

* Please submit bugs and enhancements to the `Issue Tracker
  <https://gitlab.com/pdftools/pdfjoin/issues>`_.

* You may browse the code at the
  `Repository Browser
  <https://gitlab.com/pdftools/pdfposter>`_.
  Or you may check out the current version by running ::

    git clone https://gitlab.com/pdftools/pdfposter.git


*Historical Note:*
|pdfposter| was hosted at origo.ethz.ch, which closed in May 2012.
Then |pdfposter| was hosted on gitorious.org,
which was closed in May 2015 and merged into gitlab.


.. include:: _common_definitions.txt
